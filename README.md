## RUN MySQL Source

`docker run -p 3306:3306 --name my-mysql -e MYSQL_ROOT_PASSWORD=admin -v $HOME/mysql-data:/var/lib/mysql -d mysql`


go to the mysql container and create the database

```
create database studentsDB;

use studentsDB;

create table people (id int primary key; name varchar(30));

alter table people add lastUpdated Timestamp default CURRENT_TIMESTAMP;

insert into people values (1, "lama", "2021-08-02T03:21:44+00:00");
```

## Run Sql server source

```
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Qwert#1234"    -p 1433:1433 --name sql1 -h sql1 -d mcr.microsoft.com/mssql/server:2019-latest
```

```
docker run --name SQL19 -p 1433:1433  -e "ACCEPT_EULA=Y"  -e "SA_PASSWORD=Qwert#1234"  -e "MSSQL_AGENT_ENABLED=True" -v ~/SqlDockerVol/userdatabase:/userdatabase  -v ~/SqlDockerVol/sqlbackups:/sqlbackups -d mcr.microsoft.com/mssql/server:2019-latest

```
connect to sql server via sql server mgmt

![jj](/uploads/9ae719a719d87ec96a2ffeae16c37e5c/jj.png)


create database studentsDB

then do te following

```
create table people(
id int primary key,
name varchar(30),
lastUpdated datetime2 default CURRENT_TIMESTAMP
);

insert into [dbo].[people] (id, name) values (2, 'Manal');

SELECT *
  FROM [studentsDB].[dbo].[people]
 
```

```
/****** Script for SelectTopNRows command from SSMS  ******/
create table dbo.people (id int primary key, name varchar(30));



insert into dbo.people (id, name) values (3, 'Rahaf');


SELECT *
  FROM [studentsDB].[dbo].[people]

delete people where 1=1


EXEC sys.sp_cdc_enable_db  ;

SELECT name, is_cdc_enabled
FROM sys.databases WHERE database_id = DB_ID();

EXEC sys.sp_cdc_enable_table
    @source_schema = N'dbo',
    @source_name   = N'People',
    @role_name     = NULL,
    @supports_net_changes = 1
    GO

SELECT s.name AS Schema_Name, tb.name AS Table_Name
, tb.object_id, tb.type, tb.type_desc, tb.is_tracked_by_cdc
FROM sys.tables tb
INNER JOIN sys.schemas s on s.schema_id = tb.schema_id
WHERE tb.is_tracked_by_cdc = 1


SELECT * 
FROM sys.change_tracking_databases 
WHERE database_id=DB_ID('studentsDB');

```

### pre-requesties

1- JDBC connector (you don't need to download it for this repo it's already included)

2- Mysql connector (you don't need to download it for this repo it's already included)

3- sql server connector (you don't need to download it for this repo it's already included)


write your mysql properties file

this is my naming for the file you don't need to use same as it `etc/kafka/thiqah-mysql.properties`

```
name=test-source-mysql-jdbc-event-notifier
connector.class=io.confluent.connect.jdbc.JdbcSourceConnector
tasks.max=1
connection.user=root
connection.password=admin
connection.url=jdbc:mysql://127.0.0.1:3306/DBname
# tables=[tableName]
mode=timestamp
validate.non.null=false
timestamp.column.name=lastUpdated
# incrementing.column.name=id
topic.prefix=my-prefix-
```


* Note: you must create a timestamp field in your DB so the connector can know it's new event. that's why i used `timestamp.column.name=lastUpdated` because the timestamp column in my DB was `lastUpdated`


## Run zookeeper server

`bin/zookeeper-server-start etc/kafka/zookeeper.properties`


## Run kafka server

`bin/kafka-server-start etc/kafka/server.properties`


## Run Schema Registry

`bin/schema-registry-start etc/schema-registry/schema-registry.properties`


## Run Kafka Connect standalone with mysql properties

`bin/connect-standalone etc/schema-registry/connect-avro-standalone.properties etc/kafka/my-mysql.properties`


## Run avro consumer to see all the updates that will happen in Mysql

`bin/kafka-avro-console-consumer --topic my-prefix-tableName --bootstrap-server localhost:9092` 




### Resources:

https://www.tutorialkart.com/apache-kafka/kafka-connector-mysql-jdbc/

https://stackoverflow.com/questions/53538802/confluent-error-failed-to-run-query-for-table-timestampincrementingtablequerier

https://stackoverflow.com/questions/53745747/confluent-jdbc-connector-cannot-make-incremental-queries-using-timestamp-columns

https://docs.confluent.io/kafka-connect-jdbc/current/source-connector/source_config_options.html

https://dev.mysql.com/downloads/connector/j/

https://www.youtube.com/watch?v=gPvwvkCVSnY

https://www.confluent.io/hub/confluentinc/kafka-connect-jdbc

https://docs.confluent.io/cloud/current/connectors/cc-microsoft-sql-server-source.html
